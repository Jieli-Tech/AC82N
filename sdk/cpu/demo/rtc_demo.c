#include "asm/power/rtc.h"
#include "timer.h"
#include "asm/power_interface.h"

struct sys_time temp;
u16 dump_timer;

void alm_ring_fun(void)
{
    printf("\nalarm ring di~di~di~\n");
}

void rtc_time_test_dump(void)
{
    rtc_read_time(&temp);
    printf("rtc_sys_time: %d-%d-%d %d:%d:%d\n", temp.year, temp.month, temp.day, temp.hour, temp.min, temp.sec);
}

void rtc_test_demo_start(void)
{
    struct sys_time def_alarm_time = {
        .year = 2023,
        .month = 1,
        .day = 1,
        .hour = 8,
        .min = 0,
        .sec = 10,
    };

    struct sys_time def_rtc_time = {
        .year = 2023,
        .month = 1,
        .day = 1,
        .hour = 0,
        .min = 0,
        .sec = 0,
    };
    struct rtc_config_init rtc = {
        .default_sys_time = &def_rtc_time,
        .default_alarm = &def_alarm_time,
        .rtc_clk    = CLK_SEL_LRC,
        .rtc_sel    = VIR_RTC,
        .alm_en     = 1,
        .cbfun      = alm_ring_fun,
        /* .timer_wkup_en = 1, */
    };

    rtc_dev_init(&rtc);
    printf("---------------\n");
    printf("\nrtc_dev_init\n");
    rtc_read_time(&temp);
    printf("rtc_default_sys_time: %d-%d-%d %d:%d:%d\n", temp.year, temp.month, temp.day, temp.hour, temp.min, temp.sec);

    rtc_read_alarm(&temp);
    printf("rtc_default_alarm: %d-%d-%d %d:%d:%d\n", temp.year, temp.month, temp.day, temp.hour, temp.min, temp.sec);
    printf("---------------\n");

    dump_timer = usr_timer_add(NULL, (void *)rtc_time_test_dump, 1000, 0);
}

void rtc_reset_alarm_presoftoff_test(void)
{
    if (!dump_timer) {
        return;
    }
    // sofyoff前设置闹钟
    rtc_read_time(&temp);
    if (temp.sec < 55) {
        temp.sec += 5;
    } else {
        temp.min += 1;
        temp.sec -= 55;
    }
    rtc_write_alarm(&temp);

    // 休眠 等待闹钟唤醒
    /* sys_softoff(); */
}

void rtc_test_demo_close(void)
{
    if (dump_timer) {
        usr_timer_del(dump_timer);
        dump_timer = 0;
    }
    rtc_dev_disable();
}
