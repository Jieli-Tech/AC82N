#ifndef _UI_IMAGE_
#define _UI_IMAGE_

#include "typedef.h"
#include "ui_resfile.h"

//PAGE 0

//PAGE 1
#define  PAGE1_1238_RED_0            0x010001 //  config\oximeter\15x35_0.bmp
#define  PAGE1_7d7d_RED_1            0x010002 //  config\oximeter\15x35_1.bmp
#define  PAGE1_ccb2_RED_2            0x010003 //  config\oximeter\15x35_2.bmp
#define  PAGE1_a3f7_RED_3            0x010004 //  config\oximeter\15x35_3.bmp
#define  PAGE1_bf0d_RED_4            0x010005 //  config\oximeter\15x35_4.bmp
#define  PAGE1_d048_RED_5            0x010006 //  config\oximeter\15x35_5.bmp
#define  PAGE1_6187_RED_6            0x010007 //  config\oximeter\15x35_6.bmp
#define  PAGE1_0ec2_RED_7            0x010008 //  config\oximeter\15x35_7.bmp
#define  PAGE1_5873_RED_8            0x010009 //  config\oximeter\15x35_8.bmp
#define  PAGE1_3736_RED_9          	 0x01000a //  config\oximeter\15x35_9.bmp
#define  PAGE1_9a35_BLUE_0           0x01002a //  config\oximeter\text_0.bmp
#define  PAGE1_f570_BLUE_1           0x01002b //  config\oximeter\text_1.bmp
#define  PAGE1_44bf_BLUE_2           0x01002c //  config\oximeter\text_2.bmp
#define  PAGE1_2bfa_BLUE_3           0x01002d //  config\oximeter\text_3.bmp
#define  PAGE1_3700_BLUE_4           0x01002e //  config\oximeter\text_4.bmp
#define  PAGE1_5845_BLUE_5           0x01002f //  config\oximeter\text_5.bmp
#define  PAGE1_e98a_BLUE_6           0x010030 //  config\oximeter\text_6.bmp
#define  PAGE1_86cf_BLUE_7           0x010031 //  config\oximeter\text_7.bmp
#define  PAGE1_d07e_BLUE_8           0x010032 //  config\oximeter\text_8.bmp
#define  PAGE1_bf3b_BLUE_9           0x010033 //  config\oximeter\text_9.bmp

#define  PAGE1_5419_BG_00            0x01000b //  config\oximeter\bg_00.bmp
#define  PAGE1_3b5c_BG_01            0x01000c //  config\oximeter\bg_01.bmp
#define  PAGE1_8a93_BG_02            0x01000d //  config\oximeter\bg_02.bmp
#define  PAGE1_e5d6_BG_03            0x01000e //  config\oximeter\bg_03.bmp
#define  PAGE1_f92c_BG_04            0x01000f //  config\oximeter\bg_04.bmp

#define  PAGE1_55f0_ICON_CELL_01     0x010026 //  config\oximeter\icon_cell_01.bmp
#define  PAGE1_e43f_ICON_CELL_02     0x010027 //  config\oximeter\icon_cell_02.bmp
#define  PAGE1_8b7a_ICON_CELL_03     0x010028 //  config\oximeter\icon_cell_03.bmp
#define  PAGE1_9780_ICON_CELL_04     0x010029 //  config\oximeter\icon_cell_04.bmp

#define  PAGE1_0dd7_BLUE_5X10_0      0x010010 //  config\oximeter\blue_5x10_0.bmp
#define  PAGE1_6292_BLUE_5X10_1      0x010011 //  config\oximeter\blue_5x10_1.bmp
#define  PAGE1_d35d_BLUE_5X10_2      0x010012 //  config\oximeter\blue_5x10_2.bmp
#define  PAGE1_bc18_BLUE_5X10_3      0x010013 //  config\oximeter\blue_5x10_3.bmp
#define  PAGE1_a0e2_BLUE_5X10_4      0x010014 //  config\oximeter\blue_5x10_4.bmp
#define  PAGE1_cfa7_BLUE_5X10_5      0x010015 //  config\oximeter\blue_5x10_5.bmp
#define  PAGE1_7e68_BLUE_5X10_6      0x010016 //  config\oximeter\blue_5x10_6.bmp
#define  PAGE1_112d_BLUE_5X10_7      0x010017 //  config\oximeter\blue_5x10_7.bmp
#define  PAGE1_479c_BLUE_5X10_8      0x010018 //  config\oximeter\blue_5x10_8.bmp
#define  PAGE1_28d9_BLUE_5X10_9      0x010019 //  config\oximeter\blue_5x10_9.bmp
#define  PAGE1_febf_YELLOW_5X10_0    0x010038 //  config\oximeter\yellow_5x10_0.bmp
#define  PAGE1_91fa_YELLOW_5X10_1    0x010039 //  config\oximeter\yellow_5x10_1.bmp
#define  PAGE1_2035_YELLOW_5X10_2    0x01003a //  config\oximeter\yellow_5x10_2.bmp
#define  PAGE1_4f70_YELLOW_5X10_3    0x01003b //  config\oximeter\yellow_5x10_3.bmp
#define  PAGE1_538a_YELLOW_5X10_4    0x01003c //  config\oximeter\yellow_5x10_4.bmp
#define  PAGE1_3ccf_YELLOW_5X10_5    0x01003d //  config\oximeter\yellow_5x10_5.bmp
#define  PAGE1_8d00_YELLOW_5X10_6    0x01003e //  config\oximeter\yellow_5x10_6.bmp
#define  PAGE1_e245_YELLOW_5X10_7    0x01003f //  config\oximeter\yellow_5x10_7.bmp
#define  PAGE1_b4f4_YELLOW_5X10_8    0x010040 //  config\oximeter\yellow_5x10_8.bmp
#define  PAGE1_dbb1_YELLOW_5X10_9    0x010041 //  config\oximeter\yellow_5x10_9.bmp

#define  PAGE1_1f05_TEXT_OXIMETER    0x010034 //  config\oximeter\text_oximeter.bmp
#define  PAGE1_2a6a_TEXT_PI          0x010035 //  config\oximeter\text_pi.bmp
#define  PAGE1_12d5_TEXT_PR          0x010036 //  config\oximeter\text_PR.bmp
#define  PAGE1_422a_TEXT_SPO2        0x010037 //  config\oximeter\text_spo2.bmp

#define  PAGE1_3591_BLUE_on		     0x01001a //  config\oximeter\blue_h10_01.bmp
#define  PAGE1_845e_BLUE_off         0x01001b //  config\oximeter\blue_h10_02.bmp
#define  PAGE1_eb1b_BLUE_ADD         0x01001c //  config\oximeter\blue_h10_03.bmp
#define  PAGE1_f7e1_BLUE_DEL         0x01001d //  config\oximeter\blue_h10_04.bmp
#define  PAGE1_98a4_BLUE_Exit        0x01001e //  config\oximeter\blue_h10_05.bmp
#define  PAGE1_296b_BLUE_Alm         0x01001f //  config\oximeter\blue_h10_06.bmp
#define  PAGE1_462e_BLUE_Pulse_Beep  0x010020 //  config\oximeter\blue_h10_07.bmp
#define  PAGE1_109f_BLUE_SpO2_Lo     0x010021 //  config\oximeter\blue_h10_08.bmp
#define  PAGE1_7fda_BLUE_PR_Hi       0x010022 //  config\oximeter\blue_h10_09.bmp
#define  PAGE1_59a1_BLUE_PR_Lo       0x010023 //  config\oximeter\blue_h10_10.bmp
#define  PAGE1_36e4_BLUE_Restore     0x010024 //  config\oximeter\blue_h10_11.bmp
#define  PAGE1_872b_BLUE_ADD_DEL     0x010025 //  config\oximeter\blue_h10_12.bmp

#define  PAGE1_c6f9_YELLOW_Alm    	 0x010042 //  config\oximeter\yellow_h10_01.bmp
#define  PAGE1_7736_YELLOW_Pulse_Beep 0x010043 //  config\oximeter\yellow_h10_02.bmp
#define  PAGE1_1873_YELLOW_SpO2_Lo   0x010044 //  config\oximeter\yellow_h10_03.bmp
#define  PAGE1_0489_YELLOW_PR_Hi     0x010045 //  config\oximeter\yellow_h10_04.bmp
#define  PAGE1_6bcc_YELLOW_PR_Lo     0x010046 //  config\oximeter\yellow_h10_05.bmp
#define  PAGE1_da03_YELLOW_Restore   0x010047 //  config\oximeter\yellow_h10_06.bmp
#define  PAGE1_b546_YELLOW_ADD_DEL   0x010048 //  config\oximeter\yellow_h10_07.bmp
#define  PAGE1_e3f7_YELLOW_Exit    	 0x010049 //  config\oximeter\yellow_h10_08.bmp
#define  PAGE1_8cb2_YELLOW_on    	 0x01004a //  config\oximeter\yellow_h10_09.bmp
#define  PAGE1_aac9_YELLOW_off    	 0x01004b //  config\oximeter\yellow_h10_10.bmp
#define  PAGE1_c58c_YELLOW_ADD    	 0x01004c //  config\oximeter\yellow_h10_11.bmp
#define  PAGE1_7443_YELLOW_DEL    	 0x01004d //  config\oximeter\yellow_h10_12.bmp

int ui_show_image_by_id(u32 id, u16 x, u16 y);

#endif
