#ifndef __PUBLIC_IMG_TYPEDEF_H__
#define __PUBLIC_IMG_TYPEDEF_H__


/************************************/
/* redefine u8, u16, u32 data type. */
/************************************/

#ifndef u32
typedef unsigned int u32;
#endif

#ifndef u16
typedef unsigned short u16;
#endif

#ifndef u8
typedef unsigned char u8;
#endif


#endif
