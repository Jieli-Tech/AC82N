#ifndef _DEBUG_H_
#define _DEBUG_H_

#include "asm/cpu.h"
#include "generic/typedef.h"

void printf_buf(u8 *buf, u32 len);

#define LOG_VERBOSE     v
#define LOG_DEBUG       d
#define LOG_INFO        i
#define LOG_WARN        w
#define LOG_ERROR       e
#define LOG_CHAR        c

#define _STR(x)     #x
#define STR(x)     "["_STR(x)"]"

#define _LOG_TAG_CONST_DECLARE(level, name)         extern const char log_tag_const_##level##_##name
#define LOG_TAG_CONST_DECLARE(level, name)          _LOG_TAG_CONST_DECLARE(level, name)

#define ___LOG_IS_ENABLE(level, name)   (log_tag_const_##level##_##name)
#define __LOG_IS_ENABLE(level, name)    ___LOG_IS_ENABLE(level, name)
#define _LOG_IS_ENABLE(level)           __LOG_IS_ENABLE(level, LOG_TAG_CONST)

#ifdef LOG_TAG_CONST
LOG_TAG_CONST_DECLARE(LOG_VERBOSE,      LOG_TAG_CONST);
LOG_TAG_CONST_DECLARE(LOG_INFO,         LOG_TAG_CONST);
LOG_TAG_CONST_DECLARE(LOG_DEBUG,        LOG_TAG_CONST);
LOG_TAG_CONST_DECLARE(LOG_WARN,         LOG_TAG_CONST);
LOG_TAG_CONST_DECLARE(LOG_ERROR,        LOG_TAG_CONST);
LOG_TAG_CONST_DECLARE(LOG_CHAR,         LOG_TAG_CONST);
#ifdef LOG_TAG
#define _LOG_TAG                        LOG_TAG
#else
#define _LOG_TAG                        STR(LOG_TAG_CONST)
#endif
#define LOG_IS_ENABLE(level)            _LOG_IS_ENABLE(level)

#else
#define _LOG_TAG            LOG_TAG
#define LOG_IS_ENABLE(x)    1
#endif

/*
 *  LOG 通过常量控制
 */
#define log_info(format, ...)       \
    if (LOG_IS_ENABLE(LOG_INFO)) \
        printf("[Info]: " _LOG_TAG format "\r\n", ## __VA_ARGS__)

#define log_info_hexdump(x, y)     \
    if (LOG_IS_ENABLE(LOG_INFO)) \
        printf_buf(x, y)

#define log_debug(format, ...)       \
    if (LOG_IS_ENABLE(LOG_DEBUG)) \
        printf("[Debug]: " _LOG_TAG format "\r\n", ## __VA_ARGS__)

#define log_debug_hexdump(x, y)     \
    if (LOG_IS_ENABLE(LOG_DEBUG)) \
        printf_buf(x, y)

#define log_error(format, ...)       \
    if (LOG_IS_ENABLE(LOG_ERROR)) \
        printf("<Error>: " _LOG_TAG format "\r\n", ## __VA_ARGS__)

#define log_error_hexdump(x, y)     \
    if (LOG_IS_ENABLE(LOG_ERROR)) \
        printf_buf(x, y)

#define log_char(x)       \
    if (LOG_IS_ENABLE(LOG_CHAR)) \
		putchar(x)

#define traceSUPER_MODE()


#endif//__DEBUG_H_


