#ifndef SYS_INCLUDES_H
#define SYS_INCLUDES_H

#include "timer.h"
#include "malloc.h"
#include "event.h"
#include "spinlock.h"
#include "fs/fs.h"

#include "generic/includes.h"
#include "asm/includes.h"

#endif

