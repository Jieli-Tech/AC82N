#ifndef GENERIC_INCLUDES_H
#define GENERIC_INCLUDES_H

#include "errno-base.h"
#include "typedef.h"
#include "ascii.h"
#include "ioctl.h"
#include "jiffies.h"
#include "list.h"
#include "printf.h"
#include "circular_buf.h"
#include "lbuf.h"

#endif
