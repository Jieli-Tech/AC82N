# AC82N_GP_MCU

快速开始
------------

欢迎使用杰理AC82N系列通用MCU开源项目，在开始进入项目之前，请详细阅读以下芯片介绍获得对AC82N系列通用MCU芯片大概认识，SDK固件包不含开发文档，进行开发之前详细阅读SDK开发文档:[https://doc.zh-jieli.com/AC82/zh-cn/master/index.html](https://doc.zh-jieli.com/AC82/zh-cn/master/index.html)，为用户提供完善的开发例程，帮助开发者快速顺利地使用AC82N系列通用MCU芯片进行方案开发。如需加入技术交流社区，请搜索钉钉群号：42090002468。

通用MCU项目介绍
-------------------
通用MCU系列 SoC 芯片支持以下功能：

- 支持高精度HADC，有效精度最高可达19bit
- 支持高速SARADC，速度可达1Msps
- 超低功耗处理器
- 支持低功耗RTC，闹钟和唤醒
- 可被广泛应用于测量类、控制类、智能类产品和其他低功耗领域中。 
- 提供HADC、SARADC、IIC、SPI、MCPWM、UART、USB、触摸按键等丰富的外设接口

硬件环境
-------------
* 开发评估板 ：开发板申请入口[链接](https://shop321455197.taobao.com/?spm=a230r.7195193.1997079397.2.2a6d391d3n5udo)

* 生产烧写工具 : 为量产和裸片烧写而设计, 申请入口 [链接](https://shop321455197.taobao.com/category-1535937872.htm?spm=a1z10.5-c-s.w4010-22878283454.12.b4225d315SlUhb&search=y&catName=%D0%BE%C6%AC#bd) 

免责声明
------------

* AC82N_SDK 支持AC82N 系列芯片开发.

* AC82N 系列芯片支持了通用MCU 常见应用，可以作为开发，评估，样品，甚至量产使用，对应SDK 版本见Release