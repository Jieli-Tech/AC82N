del project\config\ini\project.ini
copy project\config\ini\project_normal.ini.bak project\config\ini\project.ini
del project\"Application Data"\ui-config
copy project\"Application Data"\ui-config_normal.bak project\"Application Data"\ui-config
cd project
if not exist ResBuilder.xml copy ..\..\..\UITools\ResBuilder.xml
if exist ..\..\..\UITools\config\ini\option.ini copy ..\..\..\UITools\config\ini\option.ini config\ini\
start ..\..\..\UITools\QtToolBin.exe